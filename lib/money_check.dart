import 'package:flutter/material.dart';

class MoneyCheck extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('บัญชีธนาคาร'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 200,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(
                      'https://media.discordapp.net/attachments/789902286569603113/1071907895073050664/IMG_3108.png'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    offset: Offset(0, 2),
                    blurRadius: 6.0,
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Text(
              'Wittaya Deesongkroh',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 5),
            Text(
              'ธนาคารกรุงไทย(Krungthai Banking)',
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey,
              ),
            ),
            SizedBox(height: 20),
            Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                    color: Colors.grey,
                    width: 0.5,
                  ),
                  bottom: BorderSide(
                    color: Colors.grey,
                    width: 0.5,
                  ),
                ),
              ),
              child: Column(
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.account_balance),
                    title: Text('Account Number'),
                    subtitle: Text('679-5-67221-2'),
                  ),
                  ListTile(
                    leading: Icon(Icons.attach_money),
                    title: Text('Balance'),
                    subtitle: Text('11130.44 Baht'),
                  ),
                  ListTile(
                    leading: Icon(Icons.history),
                    title: Text('Transaction History'),
                    subtitle: Text('View'),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
            Container(
              child: Row(
                    children: [IconButton(onPressed: () {}, icon: Icon(Icons.account_box_rounded))],
              ),
            ),
            Row(
              children: [IconButton(onPressed: () {}, icon: Icon(Icons.access_time))],
            )
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
      ),
    );
  }
}

Widget buildButtonWidget() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[buildCallButton()],
  );
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
