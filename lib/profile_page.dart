import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('ข้อมูลส่วนตัว'),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                          'https://scontent.fbkk21-1.fna.fbcdn.net/v/t39.30808-6/285092797_5226903280702897_3230250096445101765_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=8bfeb9&_nc_eui2=AeHzzFeUPjYwZxIq3g3g-ugfp7ZBqBZ4_XmntkGoFnj9eQmIDudDDVVEpxYGmrmW7hNBeFyl4UpIleArq16ssLNw&_nc_ohc=X32cGkOpvpIAX-1V1Gk&tn=RA8nMn16GGpwCMO1&_nc_ht=scontent.fbkk21-1.fna&oh=00_AfD-OGZTnF77x5pYW1ZJdtpoY7r9RW_c6qqKMFqBadDFBA&oe=63E471A1'),
                      fit: BoxFit.cover,
                    ),
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        offset: Offset(0, 2),
                        blurRadius: 6.0,
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Wittaya Deesongkroh',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  'Infomatic-CS',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                Text(
                  '63160221',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      bottom: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      ListTile(
                        leading: Icon(Icons.email),
                        title: Text('Email'),
                        subtitle: Text('63160221@go.buu.ac.th'),
                      ),
                      ListTile(
                        leading: Icon(Icons.phone),
                        title: Text('Phone'),
                        subtitle: Text('095 541 4764'),
                      ),
                      ListTile(
                        leading: Icon(Icons.location_on),
                        title: Text('Location'),
                        subtitle:
                            Text('222/78 Sukhumvit Road Bangchak Bangkok'),
                      ),
                      ListTile(
                        leading: Icon(Icons.location_on),
                        title: Text('Location'),
                        subtitle: Text('Your Address'),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                      bottom: BorderSide(
                        color: Colors.grey,
                        width: 0.5,
                      ),
                    ),
                  ),
                  child: Column(
                    children: <Widget>[
                      Table(
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.middle,
                        children: [
                          TableRow(children: [
                            TableCell(
                              child: Text(
                                "Grade",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            TableCell(
                              child: Text("3.50"),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                              child: Text(
                                "Gender",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            TableCell(
                              child: Text("Man"),
                            ),
                          ]),
                          TableRow(children: [
                            TableCell(
                              child: Text(
                                "BirthDay",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            TableCell(
                              child: Text("29 March 2002"),
                            ),
                          ]),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(
                          'https://media.discordapp.net/attachments/887544957345341440/1071901451523199066/image.png'),
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
