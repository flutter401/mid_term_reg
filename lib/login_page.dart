import 'package:flutter/material.dart';

import 'main_page.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amber.shade100,

        body: SingleChildScrollView(
        child:Center(
          child: ConstrainedBox(

            constraints: const BoxConstraints(maxWidth: 300.0),
            child: Column(

              // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [

                SizedBox(height: 50.0),
               // const Spacer(flex: 5),
                Image.network(
                  "https://yt3.googleusercontent.com/ytc/AL5GRJX50rDpHgwfegkSKhjnGufUOq9RysHpULhWz96w=s900-c-k-c0x00ffffff-no-rj",
                  width: 300,
                ),
                //const SizedBox(height: 100.0),
                //const Spacer(flex: 5),
                const TextField(
                    decoration: InputDecoration(labelText: 'Username')),
                const TextField(
                    decoration: InputDecoration(labelText: 'Password')),
              //  const Spacer(flex: 5),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children:[
                    ElevatedButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                                return MainPage();
                              }));
                        },
                        child: const Text('Login')),
                 //   const Spacer(flex: 10),
                    ElevatedButton(
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) {
                                return MainPage();
                              }));
                        },
                        child: const Text('Register')),
                  //  const Spacer(flex: 10),
                  ]
                ),
                //const SizedBox(height: 30.0),
              ],
            ),
          ),
        )));
  }
}


