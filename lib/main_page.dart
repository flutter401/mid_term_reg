import 'package:column_widget_example/navigation_drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) => Scaffold(
    drawer: NavigationDrawerWidget(),
    appBar: AppBar(
      title: Text('ข่าวกิจกรรม'),
      backgroundColor: Colors.amber,
      centerTitle: true,
    ),
    body: SingleChildScrollView(
      child:Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Card(
              color: Colors.tealAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.network(
                      'https://media.discordapp.net/attachments/887544957345341440/1071889064082542712/327765475_3409769862577905_1283848621012132516_n.png?width=992&height=701',
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.purpleAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.network(
                      'https://images-ext-2.discordapp.net/external/sGfLXcGZ_d23khWufXhox_n1hRcF_BApoXs2F9rOKXM/https/storage.googleapis.com/inskru-optimized-image/-MaDmL8yjiI35v9ZfCrf%3A0.webp?width=1052&height=701',
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ],
                ),
              ),
            ),
            Card(
              color: Colors.lightGreenAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Image.network(
                      'https://media.discordapp.net/attachments/789902286569603113/1073489868447027260/294908208_1186739828786323_6746335851164836362_n.png?width=483&height=684',
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),

    )

  );
}


