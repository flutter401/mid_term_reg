
import 'package:flutter/material.dart';
import 'drawer_item.dart';

final itemsFirst = [
  DrawerItem(title: 'ProfilePage', icon: Icons.person),
  DrawerItem(title: 'MoneyCheck', icon: Icons.currency_exchange),
  DrawerItem(title: 'CalendarPage', icon: Icons.calendar_month),
  DrawerItem(title: 'SchoolSchedul', icon: Icons.calendar_today_sharp),

];
